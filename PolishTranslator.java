public class PolishTranslator implements GameTranslator {

    @Override
    public String getPlayer1Action(ActionEnum action1) {
        return "Akcja gracza 1:" + action1;
    }

    @Override
    public String getPlayer2Action(ActionEnum action2) {
        return "Akcja gracza 2:" + action2;
    }

    @Override
    public String getTieResult() {
        return "Remis!";
    }

    @Override
    public String getPlayer1Won() {
        return "Wygrał gracz nr 1!";
    }

    @Override
    public Object getPlayer2Won() {
        return "Wygrał gracz nr 2!";
    }
}
