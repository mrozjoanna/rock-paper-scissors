import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Próbna gra losowa:");
        System.out.println();
        Player player1 = new ComputerPlayer();
        Player player2 = new ComputerPlayer();

        GameTranslator translator = new PolishTranslator();

        Game game = new Game(player1, player2, translator);
        game.start();
        System.out.println();

        String answer;
        do {

            System.out.println();
            System.out.println("Let's check your today's luck with computer..");
            System.out.println();
            Player player3 = new ScannerPlayer();
            Player player4 = new ComputerPlayer();

            GameTranslator translator2 = new EnglishTranslator();

            Game game2 = new Game(player3, player4, translator2);
            game2.start();

            Scanner scanner = new Scanner(System.in);
            System.out.println("Would you like to try once more time? [y/n]");
            answer = scanner.next();

        } while (answer.equals("y"));
    }
}
