public class Game {
    private final Player player1;
    private final Player player2;
    private GameTranslator translator;

    public Game(Player player1, Player player2, GameTranslator translator) {
        this.player1 = player1;
        this.player2 = player2;
        this.translator = translator;
    }

    public void start() {
        ActionEnum action1 = player1.chooseAction();
        ActionEnum action2 = player2.chooseAction();

        System.out.println(translator.getPlayer1Action(action1));
        System.out.println(translator.getPlayer2Action(action2));

        GameResult result = checkResult(action1, action2);
        printResult(result);
    }

    private void printResult(GameResult result) {
        switch (result) {
            case TIE:
                System.out.println(translator.getTieResult());
                break;
            case LOOSE:
                System.out.println(translator.getPlayer2Won());
                break;
            case WIN:
                System.out.println(translator.getPlayer1Won());
                break;
        }
    }

    private GameResult checkResult(ActionEnum action1, ActionEnum action2) {
        if (action1 == action2) {
            return GameResult.TIE;
        }

        if ((action1 == ActionEnum.PAPER && action2 == ActionEnum.ROCK) ||
                (action1 == ActionEnum.ROCK && action2 == ActionEnum.SCISSORS) ||
                (action1 == ActionEnum.SCISSORS && action2 == ActionEnum.PAPER))
            return GameResult.WIN;

        return GameResult.LOOSE;
    }
}
