import java.util.Scanner;

public class ScannerPlayer implements Player {
    @Override
    public ActionEnum chooseAction() {
        Scanner scanner = new Scanner(System.in);

        do {
            printHelp();
            String action = scanner.nextLine();
            switch (action) {
                case "R":
                case "r":
                    return ActionEnum.ROCK;
                case "P":
                case "p":
                    return ActionEnum.PAPER;
                case "S":
                case "s":
                    return ActionEnum.SCISSORS;
            }

            System.out.println("WRONG!");
        } while (true);
    }

    private void printHelp() {
        System.out.println("Choose action!");
        System.out.println("R,r - rock");
        System.out.println("P,p - paper");
        System.out.println("S,s - scissors");
    }
}
