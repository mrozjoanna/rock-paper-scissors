public class EnglishTranslator implements GameTranslator {
    @Override
    public String getPlayer1Action(ActionEnum action1) {
        return "Player 1 action:" + action1;
    }

    @Override
    public String getPlayer2Action(ActionEnum action2) {
        return "Player 2 action:" + action2;
    }

    @Override
    public String getTieResult() {
        return "Tie!";
    }

    @Override
    public String getPlayer1Won() {
        return "Player 1 WON!";
    }

    @Override
    public Object getPlayer2Won() {
        return "Player 2 WON!";
    }
}
