import java.util.Random;

public class ComputerPlayer implements Player {

    @Override
    public ActionEnum chooseAction() {
        Random random = new Random();
        int action = random.nextInt(3);

        switch (action) {
            default:
            case 0:
                return ActionEnum.ROCK;
            case 1:
                return ActionEnum.PAPER;
            case 2:
                return ActionEnum.SCISSORS;
        }
    }
}
