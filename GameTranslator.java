public interface GameTranslator{
    String getPlayer1Action(ActionEnum action1);

    String getPlayer2Action(ActionEnum action2);

    String getTieResult();

    String getPlayer1Won();

    Object getPlayer2Won();
}
